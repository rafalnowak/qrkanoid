import QtQuick 2.0

/** Paddle. */
Item {
    id: paddle
    width: 100
    height: 30

    property int movingStep: width * 0.66

    Rectangle {
        id: paddleBackground
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#FFB5E2" }
            GradientStop { position: 1.0; color: "#FF82A9" }
        }
        radius: 5
        border.color: "black"
        border.width: 2
    }

    Behavior on x {
        SmoothedAnimation {
            duration: 300
            velocity: 300
        }
    }

    Keys.onLeftPressed: {
        if(x > 0) {
            x -= movingStep
        }
    }

    Keys.onRightPressed: {
        if(x + width <= parent.width) {
            x += movingStep
        }
    }
}
