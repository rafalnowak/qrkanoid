import QtQuick 2.0
import QtQuick.Particles 2.0

/** Ball. */
Item {
    id: ball
    width: 50
    height: 50

    property int velocityX : 0
    property int velocityY : 0

    /** Circle. */
    Rectangle {
        anchors.fill: parent
        color: "#FFEB3A"
        border.color: "#FFB014"
        border.width: 2
        radius: width/2
    }

    /** Moving in a smooth way. */
    Behavior on x {
        SmoothedAnimation {
            velocity: 300
        }
    }

    Behavior on y {
        SmoothedAnimation {
            velocity: 300
        }
    }
}
