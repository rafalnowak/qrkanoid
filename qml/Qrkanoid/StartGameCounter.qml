import QtQuick 2.0

Text {
    id: startGameCounter

    property int value: 3

    text: value.toString()
    font.pointSize: 32
    font.bold: true

    signal finishCountdown()

    SequentialAnimation {
        id: countDownAndDisappearAnimation
        NumberAnimation {
            id: countDownAnimation
            property: "value"
            to: 0
            duration: value * 1000
            target: startGameCounter
        }
        PropertyAnimation {
            id: visibilityAnimation
            property: "opacity"
            to: 0
            duration: 300
            target: startGameCounter
        }
        onStopped: {
            finishCountdown()
        }
    }

    function countdown(countdownValue) {
        startGameCounter.value = countdownValue
        startGameCounter.opacity = 100
        startGameCounter.visible = true
        countDownAndDisappearAnimation.start()
    }
}
