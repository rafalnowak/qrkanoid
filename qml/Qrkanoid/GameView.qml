import QtQuick 2.0

/** GameView. */
Item {
    anchors.fill: parent

    property int bricksCount: 40
    signal gameLost()

    /** Game background. */
    Rectangle {
        id: background
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#CAFF7C" }
            GradientStop { position: 1.0; color: "#60FF8B" }
        }
    }

    Grid {
        id: bricksGrid
        anchors.fill: parent
        columns: 8
        spacing: 0
        x: 0
        y: 0
        property int activeBricks: bricksCount

        Repeater {
            id: bricksRepeater
            model: bricksCount
            Brick {
                width: parent.width / parent.columns
                onDestroyedChanged: {
                    if(destroyed == true) {
                        opacity = 0
                        bricksGrid.activeBricks--
                    }
                }
            }
        }
    }

    Ball {
        id: ball
        x: -50
        y: -50
    }

    Paddle {
        id: paddle
        x: (parent.width - width)/2
        y: parent.height - height - 5
        width: parent.width * 0.15
        focus: true
    }

    Timer {
        id: gameTimer
        interval: 16
        running: false
        repeat: true
        onTriggered: {
            moveBall()
        }
    }

    states: [
        State {
            name: "NOT_RUNNING"
        },
        State {
            name: "RUNNING"
        },
        State {
            name: "LOST"
        },
        State {
            name: "WON"
        }

    ]

    function startTimer() {
        gameTimer.start()
    }

    function moveBall() {
        if(state == "RUNNING") {
            checkBallCollisionWithBordersAndChangeVelocity()
            checkBallCollisionWithPaddleAndChangeVelocity()
            checkBallCollisionWithBricks()
            checkForWonCondition()
            ball.x += ball.velocityX
            ball.y += ball.velocityY
        }
    }

    function checkBallCollisionWithBordersAndChangeVelocity() {
        if(ball.x < 0) {
            ball.velocityX *= -1
            ball.x = 0
        }
        if(ball.x > background.width - ball.width) {
            ball.velocityX *= -1
            ball.x = background.width - ball.width
        }
        if(ball.y < 0) {
            ball.velocityY *= -1
            ball.y = 0
        }
        if(ball.y > background.height) {
            gameOver()
        }
    }

    function gameOver() {
        gameTimer.stop()
        gameView.state = "LOST"
        gameLost()
    }

    function checkBallCollisionWithPaddleAndChangeVelocity() {
        if(ball.x > paddle.x && ball.x + ball.width < paddle.x + paddle.width) {
            if(ball.y + ball.height > paddle.y) {
                ball.velocityY *= -1
                ball.y = paddle.y - ball.height
            }
        }
    }

    function checkBallCollisionWithBricks() {
        for(var i = bricksRepeater.count - 1; i >= 0; --i) {
            var brick = bricksRepeater.itemAt(i)
            if(brick.destroyed === false && isBallCollidingWithBrick(brick)) {
                brick.destroyed = true
                ball.velocityY *= -1
                break
            }
        }
    }

    function isBallCollidingWithBrick(brick) {
      return !(brick.x > ball.x + ball.width ||
               brick.x + brick.width < ball.x ||
               brick.y > ball.y + ball.height ||
               brick.y + brick.height < ball.y);
    }

    function checkForWonCondition() {
        if(bricksGrid.activeBricks == 0) {
            gameView.state = "WON"
        }
    }

    function startNewGame() {
        setPaddleFocus(true)
        state = "RUNNING"
        resetBallPositionAndVelocity()
    }

    function setPaddleFocus(focus) {
        paddle.focus = focus
    }

    function resetBallPositionAndVelocity() {
        ball.x = (gameView.width - ball.width)/2
        ball.y = gameView.height * 0.6
        ball.velocityX = (Math.random()*3)+2
        ball.velocityY = (Math.random()*2)+1
    }
}
