import QtQuick 2.0

/** Main. */
Item {
    id: mainView
    width: 800
    height: 600

    GameView {
        id: gameView
        state: "NOT_RUNNING"
        onStateChanged: {
            if(state == "WON") {
                gameText.opacity = 100
                gameText.text = "You won! :)"
            }
        }

        onGameLost: {
            gameText.opacity = 100
            gameText.text = "You lost :("
        }
    }

    /** Text for game start. */
    Text {
        id: gameText
        text: "Press spacebar to start the game!"
        font.pointSize: 26
        font.bold: true
        anchors.centerIn: parent
        focus: true

        PropertyAnimation {
            id: newGameDisappearAnimation
            property: "opacity"
            to: 0
            duration: 250
            target: gameText
        }
    }

    StartGameCounter {
        id: startGameCounter
        anchors.centerIn: parent
        visible: false
        onFinishCountdown: {
            gameView.startTimer()
        }
    }

    Keys.onSpacePressed: {
        if(gameView.state == "NOT_RUNNING" || gameView.state == "LOST") {
            startNewGame()
        }
    }

    Keys.onEscapePressed: {
        Qt.quit()
    }

    function startNewGame() {
        newGameDisappearAnimation.start()
        startGameCounter.countdown(3)
        gameView.startNewGame()
    }
}
