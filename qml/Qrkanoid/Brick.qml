import QtQuick 2.0

/** Brick. */
Item {
    width: 90
    height: 40

    property bool destroyed: false

    Rectangle {
        id: tileBackground
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#FF0004" }
            GradientStop { position: 1.0; color: "#FF9F19" }
        }
        radius: 5
        border.color: "black"
        border.width: 2
    }

    Behavior on opacity {
        PropertyAnimation {
            duration: 300
        }
    }
}
